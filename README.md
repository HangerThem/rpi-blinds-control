# Blinds control using Raspberry Pi and 5V stepper motor 

- [Blinds control using Raspberry Pi and 5V stepper motor](#blinds-control-using-raspberry-pi-and-5v-stepper-motor)
  - [About](#about)
  - [How it works](#how-it-works)
    - [Calculations](#calculations)
  - [Setup](#setup)


## About

This project contains a web server used to control the 5V stepper motor conected to your Raspberry Pi.


## How it works

The Raspberry Pi runs a web server with basic control interface. When you press a button on web, it makes an HTTP request with JSON body to the server that will exectute functions controlling the stepper motor thus moving the blinds or changing the controller settings.

### Calculations

I used the 28BYJ-48 Stepper motor with ULN2003 driver board.

This motor has 63.68395:1 gear ratio even though the manufacturer says it is 64:1. It also has 4 coils inside, for more about this motor and stepper motors in general watch [this video](https://www.youtube.com/watch?v=Dc16mKFA7Fo). 


One step takes $`11.25\degree`$ when using the `full step` mode meaning 32 steps is equal to 1 revolution - $`\frac{360\degree}{11.25\degree}=32`$.

Applying the ratio $`\frac{63.68395 \times 32}{4}\approx509`$ steps per 1 revolution.
Explanation: $`\frac{GearRatio \times Steps}{NumberOfCoils}`$.

I fitted the motor to my [remixed JYSK blinds holder](https://www.prusaprinters.org/prints/108290-motor-on-jysk-roller-blinds-with-608zz-bearing-and) which uses a 2 gear train with ratio 2:1.

The gear ratio of holder I used is calculated using $`\frac{38}{19}=2`$. Explanation: $`\frac{Driven Gear Teeth Count}{Drive Gear Teeth Count}`$. This means to make a full revolution we need to make 1018 steps. To calculate how many steps is 1 cm I used the following equasion: $`\frac{360 \times 10 \times 63.68395}{\pi \times 205 \times4}\approx89`$. Explanation: $`\frac{360 \times DistanceInMm \times MotorGearRatio}{\pi \times GearOnMotorDiameter \times NumberOfCoils}`$.


## Setup

To control your blinds using this project you need to have the following:
- 1x Raspberry Pi that can connect to Wi-FI
- 1x 5V stepper motor *(I used 28BYJ-48)*
- 1x Control set for stepper motor
- 1x GPIO male header *(in case you are using Rpi Zero as I did)*
- 6x Female / Female Wire Jumpers
- 1x SD Card with Raspberry Pi OS

![Motor Circuit](media/motor_circuit.jpg "Motor Circuit")

Start with flashing Raspberry Pi OS on your SD card, insert it into the Raspberry and connect the stepper motor to the control set. Now configure your Raspberry via `sudo raspi-config` and make sure to have the correct time.
Next connect the control set to Raspberry Pi using the wire jumpers, I used the folowing pins *(not the same as on image)*:

- ULN2003 IN1 to GPIO 17
- ULN2003 IN2 to GPIO 27
- ULN2003 IN3 to GPIO 22
- ULN2003 IN4 to GPIO 23

I powered the stepper motor directly from the Raspberry Pi although it is better to have separate power source for it.


<!-- TODO: make bash instalation file and explain it here -->
<!-- TODO: allow python to use port 80 -->