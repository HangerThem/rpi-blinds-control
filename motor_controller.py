import asyncio, os, json, pause
from time import sleep
from dateutil import parser
from datetime import datetime

# import RPi.GPIO as GPIO\
# from RpiMotorLib import RpiMotorLib

# EDIT BELOW IF NEEDED!
default_settings = {
    "open_time": "08:00",
    "close_time": "20:00",
    "pins": [26, 19, 13, 6],
    "inverted": False,
    "steps_per_1cm": 88,
    "position": 0,
    "open": True,
    "window_height_cm": 100,
    "active": False,
}
motor_gear_diameter = 10.25  # in mm
data_filename = "config.json"  # MUST BE JSON!
# EDIT ABOVE IF NEEDED!

data_path = os.path.join(os.path.curdir, data_filename)

# TODO: location tracker (to close window only by certain value when for example in the middle)
class MotorController:
    """Class used to control the stepper motor and run it at given time."""

    def __init__(self) -> None:
        self.loop = asyncio.get_event_loop()

        # Check if config file exists and is not empty, otherwise load default data
        try:
            with open(data_path, "r", encoding="utf-8") as f:
                try:
                    data: dict = json.load(f)
                    if len(data) == len(default_settings.keys()):
                        self.settings: dict = data
                    else:
                        self.settings = default_settings
                except:
                    self.settings = default_settings
        except:
            self.settings = default_settings
        finally:
            self.store_settings()

    # motor = RpiMotorLib.BYJMotor()
    __is_motor_running = False

    @property
    def open_time(self) -> datetime:
        return parser.parse(self.settings["open_time"])

    @property
    def close_time(self) -> datetime:
        return parser.parse(self.settings["close_time"])

    def setup_pins(self) -> None:
        """Setups GPIO pins on the Raspberry Pi."""
        # GPIO.setmode(GPIO.BCM)
        # for pin in self.settings["pins"]:
        #     GPIO.setup(pin, GPIO.OUT)
        return

    def free_pins(self) -> None:
        """Cleanups the GPIO pins on the Raspberry."""
        # GPIO.cleanup()
        return

    def store_settings(self) -> None:
        """Writes current state to file in case server gets rebooted."""
        print("Writing")
        with open(data_path, "w", encoding="utf-8") as f:
            f.write(json.dumps(self.settings))
        print("Writing done")

    def open(self) -> None:
        """Opens window and runs store_settings."""
        self.move_by_value(100)
        self.settings["open"] = True
        self.store_settings()
        print("Opened.")

    def close(self) -> None:
        """Closes window and runs store_settings."""
        self.move_by_value(-100)
        self.settings["open"] = False
        self.store_settings()
        print("Closed.")

    def move_by_value(self, value: int) -> None:
        """Moves motor by entered amount in cm."""
        # Ignore if motor is moving otherwise it would stuck the motor.
        if self.__is_motor_running:
            return

        self.setup_pins()
        self.__is_motor_running = True

        # steps = round((abs(value) / motor_gear_radius) * 11.25)
        # inverted = (True if value < 0 else False) ^ self.settings["inverted"]
        # self.motor.motor_run(
        #     self.settings["pins"],
        #     0.002,
        #     steps,
        #     inverted,
        #     False,
        #     steptype="full",
        # )

        print(f"Moved by {value}")
        # GPIO.cleanup()
        self.__is_motor_running = False

    def reset_to_default(self) -> None:
        """Resets settings to default."""
        self.settings = default_settings
        self.store_settings()

    def update_settings(self, updated: dict) -> None:
        """Updates settings by the passed dictionary."""
        self.settings = {**self.settings, **updated}
        self.store_settings()

    async def run(self) -> None:
        """Async method to watch time and open/close on times set in settings."""
        if datetime.now() > self.open_time and datetime.now() < self.close_time:
            print(f"Waiting untill {self.close_time}.")
            # pause.until(self.close_time)
            self.close()
        else:
            print(f"Waiting untill {self.open_time}.")
            # pause.until(self.open_time)
            self.open()

    def start_auto(self) -> None:
        """Starts the controller which will automatically open/close on times set in settings."""
        if self.loop.is_running():
            return

        self.setup_pins()
        self.loop.run_until_complete(self.run())
        self.settings["active"] = True
        self.store_settings()

    def stop_auto(self) -> None:
        """Stops the controller which will no longer automatically open/close on times set in settings."""
        if self.loop.is_closed():
            return

        self.loop.stop()
        self.settings["active"] = False
        self.store_settings()
        # GPIO.cleanup()

    def stop_movement(self) -> None:
        # Do nothing when not moving
        if not self.__is_motor_running:
            return

        # try:
        #     self.motor.stop_motor = True
        #     pass
        # except RpiMotorLib.StopMotorInterrupt:
        #     Motor stopped as expected
        #     pass
        print("Motor stopped")
