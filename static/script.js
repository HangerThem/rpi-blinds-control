async function sendRequest(method, url, data) {
	return fetch(url, {
		method: method,
		headers: { "Content-Type": "application/json" },
		body: JSON.stringify(data),
	});
}

document.querySelectorAll("#movement").forEach((item) => {
	item.addEventListener("click", () => {
		let checkedRange = document.querySelector("input[name=range]:checked");
		item.value = "";
		if (item.classList.contains("down")) {
			item.value = "-";
		}
		item.value += checkedRange.value;
	});
});

document.querySelectorAll(":is(.movement .btn)").forEach((button) => {
	button.addEventListener("click", (btn) => {
		var clickedButton = btn.target;
		sendRequest("PATCH", "/control", {
			command: clickedButton.name,
			value: Number(clickedButton.value),
		});
	});
});

document.getElementById("settings-save").addEventListener("click", () => {
	const obj = {};
	document.querySelectorAll("td input").forEach((e) => {
		obj[e.name] =
			e.type === "checkbox"
				? e.checked
				: e.type === "number"
				? Number(e.value)
				: e.value;
	});
	sendRequest("POST", "/settings", obj).then((res) => {
		res.text().then((data) => alert(data));
	});
});

document.getElementById("settings-reset").addEventListener("click", () => {
	if (confirm("Your settings will be set do default")) {
		sendRequest("POST", "/settings", { reset: true }).then(() => {
			location.reload(true);
		});
	}
});

document.getElementById("active").addEventListener("click", function () {
	if (this.value === "true") {
		this.value = false;
		this.innerHTML = "Turn on";
	} else {
		this.value = true;
		this.innerHTML = "Turn off";
	}
	sendRequest("PATCH", "/control", {
		command: this.name,
		value: this.value === "true",
	});
});

document.getElementById("btn-main").addEventListener("click", function () {
	if (this.value === "true") {
		this.value = false;
		this.innerHTML = "Open";
	} else {
		this.value = true;
		this.innerHTML = "Close";
	}
	sendRequest("PATCH", "/control", {
		command: this.name,
		value: this.value === "true",
	});
});

let settingsIsOpen = false;
document.getElementById("settings-toggler").addEventListener("click", () => {
	let settings = document.getElementById("settings");
	let icon = document.getElementById("header-icon");

	if (!settingsIsOpen) {
		settings.style.display = "table";
		icon.style.transform = "rotate(180deg)";
		settingsIsOpen = true;
	} else {
		settings.style.display = "none";
		icon.style.transform = "rotate(0deg)";
		settingsIsOpen = false;
	}
});
