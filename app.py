from flask import Flask, Response, request, render_template
from motor_controller import MotorController


app = Flask(__name__)
mc = MotorController()


@app.route("/", defaults={"path": ""}, methods=["GET"])
@app.route("/<path:path>", methods=["GET"])
def index(path: str):
    return render_template(
        "index.html",
        open=mc.settings["open"],
        open_time=mc.settings["open_time"],
        close_time=mc.settings["close_time"],
        steps_per_1cm=mc.settings["steps_per_1cm"],
        inverted=mc.settings["inverted"],
        window_height_cm=mc.settings["window_height_cm"],
        active=mc.settings["active"],
    )


def try_get_json():
    if not request.is_json:
        return Response(response="Request body is not JSON", status=400)
    return request.get_json()


@app.route("/control", methods=["PATCH"])
def handle_control():
    data: dict = try_get_json()

    if {"command", "value"} != data.keys():
        return Response(response="Invalid JSON", status=400)

    command = data["command"]
    value = data["value"]

    if command == "open":
        if not isinstance(value, bool):
            return Response(response=f"Invalid value type: {type(value)}", status=400)
        mc.open() if value else mc.close()

    elif command == "active":
        if not isinstance(value, bool):
            return Response(response=f"Invalid value type: {type(value)}", status=400)
        mc.start_auto() if value else mc.stop_auto()

    elif command == "move":
        if not isinstance(value, int):
            return Response(response=f"Invalid value type: {type(value)}", status=400)
        mc.move_by_value(value)

    elif command == "stop":
        if not isinstance(value, bool):
            return Response(response=f"Invalid value type: {type(value)}", status=400)
        mc.stop_movement()

    else:
        return Response(response="Invalid command", status=400)

    return Response(status=200)


@app.route("/settings", methods=["POST"])
def modify_settings():
    data: dict = try_get_json()

    if "reset" in data.keys() and data["reset"]:
        mc.reset_to_default()
    elif "reset" not in data.keys():
        mc.update_settings(data)
    return Response(status=200, response="Saved")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
